//================================================= Empleado
$('#numeroEmpleadoInput').autocomplete({
  autoFocus: true,
  source: function (request, response) {
    $.ajax({
      url: 'https://639246afac688bbe4c608302.mockapi.io/api/empleado', //[TODO]: Cambiar ruta a la correcta
      data: {
        numEmpleado: request.term,
      },
      success: function (data) {
        if (data.length === 0) {
          Swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'No se encontró el usuario intente de nuevo',
          });
          $('#numEmpleadoInput').val('');
        }
        var transformedEmployee = $.map(data, function (employee) {
          return {
            //[TODO]: Cambiar por el verdadero dato
            label: `${employee.nombre} (${employee.numEmpleado})`,
            id: employee.numEmpleado,
            almacenista: employee.almacenista,
            planta: employee.planta,
          };
        });
        response(transformedEmployee);
      },
      error: function () {
        response(null);
      },
    });
  },
  minLength: 2,
  delay: 100,
  select: function (event, employee) {
    //Llenar almacenista
    $('#almacenistaInput').val(employee.item.almacenista);

    //Llenar capturista
    $('#plantaInput').val(employee.item.planta);

    //Cambiar el focus
    $('#addRecordButton').focus();
  },
});

//================================================= Herramienta
$('#herramientaInput').autocomplete({
  autoFocus: true,
  source: function (request, response) {
    $.ajax({
      url: 'https://639246afac688bbe4c608302.mockapi.io/api/equipo', //[TODO]: Cambiar ruta a la correcta
      data: {
        codigo: request.term,
      },
      success: function (data) {
        if (data.length === 0) {
          Swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'No se encontró la herramienta intente de nuevo',
          });
          $('#herramientaInput').val('');
        }
        var transformedTool = $.map(data, function (tool) {
          return {
            //[TODO]: Cambiar por el verdadero dato
            label: `${tool.codigo}`,
            id: tool.codigo,
            descripcion: tool.descripcion,
          };
        });
        response(transformedTool);
      },
      error: function () {
        response(null);
      },
    });
  },
  minLength: 2,
  delay: 100,
  select: function (event, tool) {
    //Llenar almacenista
    $('#descriptionInput').val(tool.item.descripcion);

    //Cambiar el focus
    $('#cantidadInput').focus();
  },
});

//================================================= Tipo de cambio
$('#precioUSDInput, #precioInput').blur(function(){
  const precioInputValue = $('#precioInput').val().trim();
  const precioUSDInputValue = $('#precioUSDInput').val().trim();

  //Revisar que ambos campos tengan un valor
  console.log(precioUSDInputValue >= precioInputValue); //WTF?
  if(precioInputValue === '' || precioUSDInputValue === '') {
    $('#tipoCambioInput').val(0);
  } else {
    //Revisar que el precioUSD sea menor o igual al precio
    $('#tipoCambioInput').val((precioInputValue/precioUSDInputValue).toFixed(2));
  }
})