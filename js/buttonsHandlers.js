//====================================================== Botón nuevo registro
$('#newRecordButton').click(function () {
  formularioHandler('abrir');
});

//====================================================== Botón cancelar registro
function cancelRecordHandler() {
  //Agregar row completo de botones
  $('#saveButtonsGroup').removeClass('d-none');

  //Cerrar el formulario
  formularioHandler('cerrar');

  //Limpiar el formulario
  limpiarFormulario('#formDocumentos');

  //Reversar los estilos de validación
  $('input,textarea').removeClass('empty-mandatory');
}

//====================================================== Botón crear registro
function addRecordHandler() {
  //Validación de campos obligatorios llenos
  if (validarFormulario('documentos') > 0) return;

  //Deshabilitar los botones del formulario
  actionFormButtonsHandler('deshabilitar', 'agregar');

  //Mandar información
  sendPOST('agregar', 'documentos');
}

//====================================================== Botones editar/consultar registro
function updateRecordHandler() {
  //Validación de campos obligatorios llenos
  if (validarFormulario('documentos') > 0) return;

  //Deshabilitar los botones del formulario
  actionFormButtonsHandler('deshabilitar', 'agregar');

  //Mandar información
  sendPOST('actualizar', 'documentos/' + $('#claveInput').val());
}

function actionButtonHandler(tipo, clave) {
  if ($(tipo).data('type') === 'edicion') {
    //Abrir el formulario
    formularioHandler('abrir');

    //Habilitar campos
    accionInputFormularios('habilitar');

    //Cambiar el botón de agregar por el de actualizar
    $('#updateRecordButton')
      .removeClass('d-none')
      .addClass('d-block')
      .prop('disabled', false);
    $('#addRecordButton').addClass('d-none');

    //Poner visible el listado de detalles
    verDetalles();

    //Poner visible el row y botón de agregar detalles
    $('#detallesButtonsGroup, #agregarDetallesButton')
      .removeClass('d-none')
      .addClass('d-block');

    //Obtener info del API
    $('#claveInput').val(clave);
    requestInfo('documentos', clave);
  } else if ($(tipo).data('type') === 'consulta') {
    //Abrir el formulario
    formularioHandler('abrir');

    //Quitar row completo de botones
    $('#saveButtonsGroup').addClass('d-none');

    //Quitar botón de agregar detalles
    $('#detallesButtonsGroup').addClass('d-none');

    //Poner visible el listado de detalles
    verDetalles();

    //Bloquear todo el formulario
    accionInputFormularios('deshabilitar');

    //Obtener info del API
    $('#claveInput').val(clave);
    requestInfo('documentos', clave);
  } else {
    //Abrir modal
    const myModalEl = $('#detallesModal').modal('show');

    //Poner la clave del documento en el hidden
    $('#claveModalInput').val(clave);
  }
}

//====================================================== Creación tabla detalles
function verDetalles() {
  if ($('#tablaDetalles').hasClass('d-none')) {
    //Agregar el row de la tabla
    $('#tablaDetalles').removeClass('d-none').addClass('d-block');

    //Obtener info de los detalles y crear tabla
    const clave = $('#claveInput').val();
    crearTablaDetalles(clave);
  } else {
    //Quitar el row de la tabla
    $('#tablaDetalles').removeClass('d-block').addClass('d-none');
    
  }
}

//====================================================== Botón de agregar detalles
function addDetallesRecordHandler() {
  //Validación de campos obligatorios llenos
  if (validarFormulario('detalles') > 0) return;

  //Deshabilitar los botones del formulario
  actionFormButtonsHandler('deshabilitarModal', 'agregar');

  //Mandar información
  sendPOST('agregarDetalles', 'documentosDetalles');
}

//====================================================== Botón para cancelar modal
function cancelModal() {
  const myModalEl = $('#detallesModal');
  const modal = bootstrap.Modal.getInstance(myModalEl);
  modal.hide();

  limpiarFormulario('#formDetalles');
}

//====================================================== Acciones reutilizables

//======================= Limpiar registro con error
function quitarErrorOnFocus() {
  $('input').focus(function () {
    $(this).hasClass('empty-mandatory') && $(this).removeClass('empty-mandatory');
  });
}

//======================= Abrir o cerrar formulario
function formularioHandler(tipo) {
  if (tipo === 'abrir') {
    //Poner visible el formulario
    $('#formDocumentos').removeClass('d-none').addClass('d-block');

    //Ocultar botón de nuevo registro
    $('#newRecordButtonContainer').removeClass('d-block').addClass('d-none');

    //Quitar el listado
    $('#rowListado').addClass('d-none');
  } else {
    //Ocultar el formulario
    $('#formDocumentos').removeClass('d-block').addClass('d-none');

    //Habilitar los campos
    accionInputFormularios('habilitar');

    //Revisar si existe el botón de actualizar
    if ($('#updateRecordButton').hasClass('d-block')) {
      $('#updateRecordButton')
        .removeClass('d-block')
        .addClass('d-none')
        .prop('disabled', false);
      $('#addRecordButton').removeClass('d-none').addClass('d-block');
    }

    //Revisar si existe el botón de agregar detalles
    if ($('#agregarDetallesButton').hasClass('d-block')) {
      $('#agregarDetallesButton').removeClass('d-block').addClass('d-none');
    }

    //Revisar si existe el row de ver detalles
    if ($('#tablaDetalles').hasClass('d-block')) {
      $('#tablaDetalles').removeClass('d-block').addClass('d-none');
    }

    //Mostrar el botón de nuevo registro
    $('#newRecordButtonContainer').removeClass('d-none').addClass('d-block');

    //Mostrar el listado
    $('#rowListado').removeClass('d-none');
  }
}

//======================= Habilitar o deshabilitar dependiendo del tipo (agregar o actualizar)
function actionFormButtonsHandler(accion, tipo) {
  if (accion === 'habilitar') {
    //Habilitar los botones
    if (tipo === 'agregar') {
      $('#addRecordButton').prop('disabled', false);
    } else {
      $('#updateRecordButton').prop('disabled', false);
    }
    $('#cancelRecordButton').prop('disabled', false);
  } else if (accion === 'deshabilitar') {
    //Deshabilitar los botones
    if (tipo === 'agregar') {
      $('#addRecordButton').prop('disabled', true);
    } else {
      $('#updateRecordButton').prop('disabled', true);
    }
    $('#cancelRecordButton').prop('disabled', true);
  } else if (accion === 'habilitarModal') {
    //Habilitar los botones
    $('#addDetallesRecordButton').prop('disabled', false);
    $('#cancelDetallesButton').prop('disabled', true);
  } else {
    //Deshabilitar los botones
    $('#addDetallesRecordButton').prop('disabled', true);
    $('#cancelDetallesButton').prop('disabled', true);
  }
}

//======================= Acciones para habilitar o deshabilitar inputs
function accionInputFormularios(tipo) {
  $('input,textarea').each(function () {
    if (tipo === 'habilitar') {
      $(this).prop('disabled', false);
    } else {
      $(this).prop('disabled', true);
    }
  });
}

//======================= Validar inputs
function validarFormulario(tipo) {
  let emptyInputs = 0;
  if (tipo === 'documentos') {
    $('input, textarea')
      .filter('[required]')
      .not('[readonly]')
      .not('.modal-input')
      .each(function () {
        if ($(this).val().trim() === '') {
          emptyInputs++;
          $(this).addClass('empty-mandatory');
        }
      });
  } else {
    $('input,textarea')
      .filter('[required]')
      .not('[readonly]')
      .not(':not(.modal-input)')
      .each(function () {
        if ($(this).val().trim() === '') {
          emptyInputs++;
          $(this).addClass('empty-mandatory');
        }
      });
  }

  if (emptyInputs > 0) {
    accionInputFormularios('habilitar');
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Favor de llenar los campos vacíos marcados en rojo',
    });
  }

  return emptyInputs;
}

//======================= Limpiar formulario
function limpiarFormulario(elemento) {
  $(elemento)[0].reset();
}

//======================= Mandar información
function sendPOST(tipo, apiTable) {
  let titleSuccess = 'Registro ';
  let habilitar = 'form';
  let method = 'POST';
  let data = {};
  if (tipo === 'agregar' || tipo === 'actualizar') {
    tipo === 'agregar' ? (titleSuccess += 'guardado') : (titleSuccess += 'actualizado');

    data = {
      docNum: $('#docNumInput').val().trim(),
      period: $('#periodInput').val().trim(),
      instance: $('#instanceInput').val().trim(),
      series: $('#seriesInput').val().trim(),
      handwrtten: $('#handwrttenInput').val().trim(),
      canceled: $('#canceledInput').val().trim(),
      object: $('#objectInput').val().trim(),
      logInst: $('#logInstInput').val().trim(),
      userSign: $('#userSignInput').val().trim(),
      transfered: $('#transferedInput').val().trim(),
      status: $('#statusInput').val().trim(),
      createDate: '',
      createTime: 0,
      updateDate: '',
      updateTime: 0,
      dataSource: $('#dataSourceInput').val().trim(),
      tipoDocumento: $('#tipoDocumentoInput').val().trim(),
      fecha: $('#fechaInput').val().trim(),
      numeroEmpleado: $('#numeroEmpleadoInput').val().trim(),
      almacenista: $('#almacenistaInput').val().trim(),
      planta: $('#plantaInput').val().trim(),
      clave: 0,
    };

    //Cambiar el método y URL
    if (tipo === 'actualizar') {
      method = 'PUT';
    }
  } else {
    //tipo === agregarDetalles -> modal
    habilitar = 'modal';
    titleSuccess += 'guardado';

    data = {
      consecutivo: 0,
      visOrder: $('#vistOrderInput').val().trim(),
      object: $('#objectInput').val().trim(),
      logInst: 0, //[TODO] num de Empleado?
      fechaMovimiento: $('#fechaInput').val().trim(),
      codigoMovimiento: $('#codMovInput').val().trim(),
      numeroHerramienta: $('#herramientaInput').val().trim(),
      cantidad: $('#cantidadInput').val().trim(),
      precio: $('#precioInput').val().trim(),
      precioUltimaCompraDolar: $('#precioUSDInput').val().trim(),
      tipoCambioDolar: $('#tipoCambioInput').val().trim(),
      clave: $('#claveModalInput').val().trim(),
    };
  }

  $.ajax({
    type: method,
    url: `https://639246afac688bbe4c608302.mockapi.io/api/${apiTable}`, //[TODO]: Cambiar ruta a la correcta
    data: data,
    dataType: 'json',
    success: function (data) {
      //AQUI ESTA EL OBJETO
      console.log('SUCCESS : ', data);

      if (habilitar === 'form') {
        //Habilitar los botones de nuevo y limpiar el formulario
        actionFormButtonsHandler('habilitar', 'agregar');

        //Limpiar formulario
        limpiarFormulario('#formDocumentos');

        //Actualizar la tabla
        tablaListado.ajax.reload();
      } else {
        //Limpiar formulario
        limpiarFormulario('#formDetalles');
      }

      //Prompt que acción realizar?
      if (tipo === 'agregar') {
        //Alerta de registro guardado
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1500,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
          },
        });

        Toast.fire({
          icon: 'success',
          title: titleSuccess,
        });

        Swal.fire({
          title: '¿Qué quieres hacer?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Crear nuevo documento',
          cancelButtonText: 'Regresar al listado',
        }).then((result) => {
          if (result.isConfirmed) {
            //Nuevo documento
            Swal.close();
          } else {
            //Regresar al listado
            formularioHandler('cerrar');
          }
        });
      } else {
        Swal.fire({
          icon: 'success',
          title: titleSuccess,
          showConfirmButton: false,
          timer: 1500,
        });

        if (habilitar === 'modal') {
          //Cerra modal
          cancelModal();
        }

        //Regresar al listado
        formularioHandler('cerrar');
      }
    },
    error: function (e) {
      console.log('ERROR : ', e);

      if (habilitar === 'form') {
        //Habilitar los botones de nuevo
        actionFormButtonsHandler('habilitar', 'agregar');
      } else {
        //Habilitar los botones de nuevo
        actionFormButtonsHandler('habilitarModal', '');
      }

      //Alerta de error
      return Swal.fire({
        icon: 'error',
        title: 'No se guardó su registro contacte a al administrador del sistema',
        showConfirmButton: false,
        timer: 1500,
      });
    },
  });
}

//======================= Solicitar información
function requestInfo(apiTable, clave) {
  $.ajax({
    type: 'GET',
    url: `https://639246afac688bbe4c608302.mockapi.io/api/${apiTable}/${clave}`, //[TODO]: Cambiar ruta a la correcta
    dataType: 'json',
    success: function (data) {
      const newData = Object.keys(data)
        .filter((key) => !key.includes('createDate'))
        .filter((key) => !key.includes('createTime'))
        .filter((key) => !key.includes('updateDate'))
        .filter((key) => !key.includes('updateTime'))
        .reduce((obj, key) => {
          return Object.assign(obj, {
            [key]: data[key],
          });
        }, {});

      Object.keys(newData).forEach(function (outerKey) {
        const selector = `#${outerKey}Input`;
        let valor = newData[outerKey];
        if (outerKey === 'fecha') {
          const fechaFormateada = new Date(newData[outerKey]);
          valor =
            fechaFormateada.getFullYear() +
            '-' +
            (fechaFormateada.getMonth() + 1) +
            '-' +
            String(fechaFormateada.getDate()).padStart(2, '0');
        }
        $(selector).val(valor);
      });
    },
    error: function (/*jqXHR, exception*/ e) {
      /*let msg = '';
      if (jqXHR.status === 0) {
        msg = 'Not connect.\n Verify Network.';
      } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
      } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
      } else if (exception === 'parsererror') {
        msg = 'Requested JSON parse failed.';
      } else if (exception === 'timeout') {
        msg = 'Time out error.';
      } else if (exception === 'abort') {
        msg = 'Ajax request aborted.';
      } else {
        msg = 'Uncaught Error.\n' + jqXHR.responseText;
      }*/
      console.log('ERROR : ', e);

      //Regreso al listado...

      //Alerta de error
      return Swal.fire({
        icon: 'error',
        title: 'No se guardó su registro contacte a al administrador del sistema',
        showConfirmButton: false,
        timer: 1500,
      });
    },
  });
}

function crearTablaDetalles(clave) {
  const tablaDetalles = $('#listadoDetalles').DataTable({
    destroy: true,
    searching: false,
    lengthMenu: [5],
    bLengthChange: false,
    responsive: {
      details: false,
    },
    language: languageTexts,
    search: {
      caseInsensitive: false,
    },
    ajax: {
      //[TODO]: Cambiar ruta a la correcta y para pasar la clave usar el primer comentario
      //url: `https://639246afac688bbe4c608302.mockapi.io/api/documentosDetalles/${clave}`,
      url: `https://639246afac688bbe4c608302.mockapi.io/api/documentosDetalles/`,
      type: 'GET',
      dataType: 'json',
      async: true,
      dataSrc: '',
    },
    deferRender: true, //https://datatables.net/examples/ajax/defer_render.html
    columnDefs: [
      //Usar este campo para el search
      {
        targets: [3, 4, 5, 6, 7],
        responsivePriority: 1,
      },
    ],
    columns: [
      {
        //LineId -> genero un consecutivo
        data: null,
        render: function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1;
        },
      },
      {
        //visOrder
        data: 'visOrder',
      },
      {
        //Object
        data: 'object',
      },
      {
        //codigoMovimiento
        data: 'codigoMovimiento',
      },
      {
        //numeroHerramienta
        data: 'numeroHerramienta',
      },
      {
        //cantidad
        data: 'cantidad',
      },
      {
        //precio
        data: 'precio',
      },
      {
        //precioUltimaCompraDolar
        data: 'precioUltimaCompraDolar',
      },
      {
        //tipoCambioDolar
        data: 'tipoCambioDolar',
      },
      {
        //fechaMovimiento
        data: 'fechaMovimiento',
        render: function (data) {
          return new Date(data).toLocaleDateString('es-MX');
        },
      },
    ],
    //Order by
    order: [[8, 'desc']],
  });
}
